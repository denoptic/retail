from django.db.models.base import ObjectDoesNotExist
from django.utils.crypto import get_random_string
from django.utils import timezone
from django.http import Http404
from tastypie.authorization import Authorization
from tastypie.resources import ModelResource
from retail.models import Check


class CheckResource(ModelResource):
    class Meta:
        queryset = Check.objects.all()
        authorization = Authorization()
        allowed_methods = ['get', 'post', 'delete']

    def get_object_list(self, request):
        return super(CheckResource, self).get_object_list(request) \
            .filter(status__exact='Обработан')

    def obj_create(self, bundle, **kwargs):
        try:
            p = float(bundle.data['price'])
            n = timezone.now()
            d = p * 0.03
            t = p - d
            Check.objects.filter(number=bundle.data['number']).update(
                    price=p, discount=d, total=t, updated_at=n)
        except:
            try:
                t = bundle.data['created_at']
            except:
                t = timezone.now()
                bundle.data['created_at'] = t
            bundle.data['updated_at'] = t
            bundle.data['number'] = 'RCPT-{0}'.format(get_random_string(4, allowed_chars='0123456789'))
            return super(CheckResource, self).obj_create(bundle=bundle, **kwargs)

    def obj_delete(self, bundle, **kwargs):
        try:
            deleted_obj = self.obj_get(bundle=bundle, **kwargs)
            deleted_obj.status = 'Отменен'
            deleted_obj.save()
        except ObjectDoesNotExist:
            raise Http404
        return deleted_obj
