from django.db import models
from decimal import Decimal


class Check(models.Model):
    """Розничный чек"""
    number = models.CharField(max_length=100, unique=True)
    status = models.CharField(max_length=100, default='Обработан')
    price = models.DecimalField(max_digits=6, decimal_places=2)
    discount = models.DecimalField(max_digits=6, decimal_places=2)
    total = models.DecimalField(max_digits=6, decimal_places=2)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    def save(self):
        self.discount = self.price * Decimal('0.03')
        self.total = self.price - self.discount
        super(Check, self).save()

    def __str__(self):
        return self.number
